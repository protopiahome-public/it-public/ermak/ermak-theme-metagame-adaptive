<?php get_header();?>	
<?php
$cur_term		= get_term_by("name", single_term_title("",0), SMC_LOCATION_NAME);
$tax_id 		= $cur_term->term_id;
$lts			= SMC_Location::get_term_meta($tax_id); // get_option("taxonomy_".$tax_id);
$lt				= $lts["location_type"];
$location_type	= get_post($lt);
?>

<div class='archive-header'>	
	<h1>
		<?php echo $location_type->picto. " <span style='font-size:0.75em;'>" .$location_type->post_title. "</span> " . $cur_term->name ?>
	</h1>
	<?php the_archive_description(); ?>
</div>
	<div id="loop-container" class="loop-container">
		<div class='location-tamplate-content'>
			<?php echo apply_filters("smc_location_archive_title", "", $cur_term); ?>
		</div>
		<?php
		
		if (have_posts()) : 	
			while ( have_posts() ) :
				the_post();
				unlimited_get_content_template();
			endwhile;
		endif;
		?>
	</div>

<?php the_posts_pagination(); ?>

<?php get_footer(); ?>