<?php
global $Soling_Metagame_Constructor;
$categories = get_the_category( $post->ID );
$separator  = ', ';
$text     = "";//__( 'Posted in', 'unlimited' ) . ' ';

$html		.= '<p class="post-categories">';
if ( $categories ) {
	foreach ( $categories as $category ) 
	{
		if ( $category === end( $categories ) && $category !== reset( $categories ) ) 
		{
			$output .= __( 'and', 'unlimited' ) . ' ';
		}
		$output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s", 'unlimited' ), $category->name ) ) . '">' . $category->cat_name . '</a>' . $separator;
	}
	$cats	.=  trim( $output, $separator );
}
$locs = "";
if( is_plugin_active('Ermak/Ermak.php') )
{
	$locations	= get_the_terms( $post->ID, SMC_LOCATION_NAME );
	if($locations)
	{
		foreach ( $locations as $location ) 
		{
			$link		= SMC_Location::get_term_link( $location->term_id );
			$lt			= $Soling_Metagame_Constructor->get_location_type( $location->term_id );		
			$output2 	.= '<a href="' . esc_url( $link ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s", 'unlimited' ), $location->name ) ) . '">' .$lt->picto . " " . $location->name . '</a>' . $separator;
		}	
		$locs	.=  trim( $output2, $separator );
	}
}
/**/
$html	.= $text . $cats . " " . $locs . "</p>";
echo $html;